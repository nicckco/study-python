def SQL_URI():
    USERNAME = 'root'
    PASSWORD = '123456'
    HOSTNAME = 'localhost'
    PORT = '3306'
    DATABASE = 'new'

    DB_URI = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(
        USERNAME,
        PASSWORD,
        HOSTNAME,
        PORT,
        DATABASE
    )
    return DB_URI