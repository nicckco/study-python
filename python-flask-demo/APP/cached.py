import json
import time
from flask import Blueprint,render_template,request,Response
from .exts import cache

cached_route = Blueprint('cached',__name__)

header = '/cached'


@cached_route.route(header + '/')
@cache.cached(timeout=10) # 缓存的有效时间是10秒
def index():
    print('index cached=========')
    time.sleep(3)
    return 'Success'

@cached_route.before_request
def beafore():
    print('beafore_request')

    print(request.path)
    print(request.headers)
    print(request.remote_addr)

    ip = request.remote_addr
    if cache.get(ip):
        return '您的ip抵制正在实施爬虫行为'
    else:
        # 对每个ip设置一个缓存，1秒内不让重复访问
        cache.set(ip, "value",timeout =1)
    pass

@cached_route.after_request
def after(response):
    return_str = response.data.decode("utf-8")
    # return_dict = json.loads(return_str)
    print("after =====================")
    print(return_str)
    return response