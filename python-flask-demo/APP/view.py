from flask import Blueprint,render_template,request,Response,redirect,url_for,abort,session
from .models import *
import datetime

view_route = Blueprint('view',__name__)

header = '/view'

@view_route.route(header + '/')
def index():
    return render_template('view/index.html',name = 'index')


@view_route.route(header + '/home/')
def home():
    # 4. 获取cookie
    # username = request.cookies.get('loginUser')
    username = session.get('loginUser')

    data = {
        'name': 'dingding',
        'hobby': ['eating', 'sleep', 'dance'],
        'num': 15,
        'username' : username
    }

    # return render_template('view/base.html')
    return render_template('view/child1.html')
    # return render_template('view/child2.html', **data)
    # return render_template('view/home.html',**data)
    # return render_template('view/home.html',username = username)


@view_route.route(header + '/login/',methods=['GET','POST'])
def login():
    # GET：访问登陆页面
    if request.method == 'GET':
        return render_template('view/login.html')
    elif request.method == 'POST':
        pass
        # 1. 获取前端提交过来的数据
        username = request.form.get('username') # 写在括号里的username要和前端一致
        password = request.form.get('password') # 写在括号里的password要和前端一致
        # 2. 模拟登录：用户名和密码验证
        if username == 'zhangsan' and password == '321':
            # 登陆成功
            response = redirect('/view/home/')
            # 3. 设置cookie
            # cookie不能用中文
            # response.set_cookie('user',username)  # 默认浏览器关闭则cookie失效
            # 过期时间：max_age: 秒
            # response.set_cookie('user',username,max_age=3600 * 24 * 4)
            
            # response.set_cookie('loginUser',username,expires=datetime.datetime(2023,12,10))

            session['loginUser'] = username

            session.permanent = True

            return response
        else:
            return 'error password'


# 注销
@view_route.route(header + '/logout/')
def logout():
    response = redirect('/view/home/')
    # 5. 删除session
    session.pop('loginUser')
    # session.clear()     # 慎用，会删除服务器下所有session
    return response



# string 接收任何没有（‘/’）的字符串（默认）
@view_route.route(header +  '/str/<string:name>/')
def str_name(name):
    print(type(name))
    return name

# int 接收整型
@view_route.route(header + '/int/<int:name>')
def int_name(name):
    return str(name)

# float 接收浮点型
@view_route.route(header + '/float/<float:name>')
def float_name(name):
    return str(name)

# path 接受路径，可接受（‘/’）
@view_route.route(header + '/path/<path:a>/')
def path(a):
    return str(a)

@view_route.route(header + '/uuid')
def get_uuid():
    import uuid
    return str(uuid.uuid4())

# uuid 只接受uuid字符串，唯一码，一种生成规则
@view_route.route(header + '/uuid/<uuid:a>')
def get_uuid2(a):
    return str(a)

# any 可以同时指定多种路径，进行限定
@view_route.route(header + '/any/<any(a,b,c):v>/')
def any(v):
    return str(v)

@view_route.route(header + '/method/', methods = ['GET','POST'])
def method():
    return 'methods'

@view_route.route(header + '/request/',methods = ['GET','POST'])
def request_def():
    pass

    res = '''
        <ul>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$method$':      </span> {0}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$cookies$':     </span> {1}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$url$':         </span> {2}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$base_url$':    </span> {3}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$root_url$':    </span> {4}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$remote_addr$': </span> {5}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$files$':       </span> {6}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$headers$':     </span> {7}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$user_agent$':  </span> {8}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$args$':        </span> {9}</li>
            <li style='line-height:25px;padding-bottom:10px;background:#ccc'><span style='color:blue'>'$form$':        </span> {10}</li>
        </ul>
    '''.format(request.method,
                request.cookies,
                request.url,
                request.base_url,
                request.root_url,
                request.remote_addr,
                request.files,
                request.headers,
                request.user_agent,
                request.args,# param date
                request.form # form date
            )

    print(request.headers.get('token'))
    print(request.form.get('name'))

    return res


@view_route.route(header + '/responseJson/',methods=['GET','POST'])
def response_json():
    param = {
        'name':'zhangsan',
        'age':12,
        'sex':1
    }

    return param


@view_route.route(header + '/responseHtml/',methods=['GET','POST'])
def response_html():
    pass

    # 4、字定义Response对象
    html = render_template('index.html', name='xx')
    print(html,type(html))  # 打印html文件中的内容，</html> <class 'str'>

    # res = make_response(html,200)
    res = Response(html)    # 这种方式和上面效果一样
    return res



@view_route.route(header + '/redirect/',methods=['GET','POST'])
def redirect_def():
    # 3、url_for()：反向解析，通过视图函数名反过来找路由【正常是通过路由找视图函数】
    # 语法：url_for('蓝图名称.视图函数名',参数···)
    ret = url_for('view.request_def',name='aa',age=31)

    print('ret:',ret)   # ret: /request/?name=aa&age=31
    return redirect(ret)


# 异常处理
# 抛出异常：abort
@view_route.route(header + '/abort/')
def make_abort():
    # 主动抛出异常
    abort(403)  # Forbidden异常
    return 'hello abort'


@view_route.errorhandler(403) # 这里的异常要和前面抛出的异常对应，否则捕获不到！
def error_handle(e):
    print('e:',e)   # e: 403 Forbidden: You don't have the permission to access the requested resource. It is either read-protected or not readable by the server.
    return '捕获异常:' + str(e)