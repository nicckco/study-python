# __init__.py : 初始化文件，创建Flask应用
from flask import Flask
from .view import view_route
from .student import student_route
from .cached import cached_route
from .index import home
import datetime
from .urls import * 


from .exts import init_exts
from .contsant.constant import SQL_URI

def creat_app():
    app = Flask(__name__)

    # 注册蓝图
    app.register_blueprint(blueprint=view_route)
    app.register_blueprint(blueprint=home)
    app.register_blueprint(blueprint=student_route)
    app.register_blueprint(blueprint=cached_route)

    # app.debug = True

    # 这里SECRET_KEY可以自己设置，它的作用就就是对照解析session中对应的value加密值
    app.config['SECRET_KEY'] = 'abcdef123'

    # 设置session的存在时间
    app.config['PERMANENT_SESSION_LIFETIME'] = datetime.timedelta(minutes=30)


    # 配置数据库连接路径
    app.config['SQLALCHEMY_DATABASE_URI'] = SQL_URI()
    # 禁止对象追踪修改
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    #解决中文乱码的问题，将json数据内的中文正常显示
    app.config['JSON_AS_ASCII'] = False
    init_exts(app=app)

    return app

