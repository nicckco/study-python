import json
from APP.sys import MyEncoder,model_to_dict,scalars_to_list
from flask import Blueprint,render_template,request,Response,redirect,url_for,abort,session,jsonify
from .models import *


student_route = Blueprint('student',__name__)
header = '/student'


@student_route.route(header + '/')
def index():
    page = request.args.get("page")
    searchText = request.args.get("searchText")
    if page is None or page == '':
        page = 1
    if searchText is None:
        searchText = ""
    tu,pages = select(searchText = searchText,page=int(page))
    template =  render_template('student/index.html',list = tu,page = int(page),pages = pages,searchText = searchText)
    # response = make_response(template)
    return Response(template,200)


@student_route.route(header + '/do/<string:op>/',methods=['GET','POST'])
def do(op):
    match op:
        case 'edit':
            id = request.form.get("id")
            name =request.form.get("name")
            sex = request.form.get("sex")
            age = request.form.get("age")
            if(name is None or name == ""):
                abort(403,"参数不全")
            if sex is None or sex == "":
                abort(403,"参数不全")
            if age is None or age == "":
                abort(403,"参数不全")
            
            stu = Student(id = id, name = name,sex = sex,age =age)
            msg = save(stu)
            return redirect("/student/")
        case 'delete':
            id = request.form.get('id')
            if(id is None or id == ""):
                abort(403,'id不能为空')
            return delete(id)
        case 'get':
            id = request.args.get("id")
            if(id is None or id == ""):
                abort(403,'id不能为空')
            return model_to_dict(get(id))
        case 'select':
            page = request.form.get("page")
            return scalars_to_list(select(None, page=int(page)))
        case _:
            return 'error operator'


def  save(stu:Student):
    try:
        if(stu.id == None or stu.id == ''):
            stu.id = None
            db.session.add(stu)
        else:
            s = Student.query.get(stu.id)
            s.name = stu.name
            s.age = stu.age
            s.sex = stu.sex

        db.session.commit()
        return 'success'
    except Exception as e:
        db.session.rollback()
        db.session.flush()
        return 'add error:' + str(e)

def delete(id):
    try:
        s = Student.query.get(id)
        if(s is None or s.id == ""):
            abort(403,"对象不存在")
        db.session.delete(s)
        db.session.commit() 
        return 'success'
    except Exception as e:
        db.session.rollback()
        db.session.flush()
        return str(e)

def get(id)  -> Student:
    s = Student.query.get(id)
    return s


limit = 4
def select(searchText,page):
    query = Student.query
    if(searchText is not None and searchText != ""):
        query = query.filter(Student.name.contains(searchText))
        pass
    if(page == None): 
        page =1
    s = query.order_by(Student.id.desc()).paginate(page=int(page),per_page=limit,error_out=False)
    return list(s), s.pages


@student_route.errorhandler(403) # 这里的异常要和前面抛出的异常对应，否则捕获不到！
def error_handle(e):
    param ={
        "code" : e.code,
        "message" : str(e)
    }
    return json.dumps(param)