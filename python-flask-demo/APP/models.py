from dataclasses import asdict, dataclass
from json import dumps
from .exts import db

class Student(db.Model):
    # 表名
    __tablename__ = 'student'
    # 定义表字段
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(55), default=True)
    age = db.Column(db.Integer, default=1)
    sex = db.Column(db.String(10), default=True)

    def __init__(self,id,name,age,sex) :
        self.id = id
        self.name = name
        self.age = age
        self.sex = sex