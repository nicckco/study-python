from flask import jsonify
from flask_restful import Resource, fields, marshal_with,reqparse
from .models import *


# 类视图：CBV(Class Based View)
# 视图函数：FBV(Function Based View)
class HellowResouce(Resource):  # 此函数的路由就统一写在了urls中
    def get(self):  # 重写Resource父类中的get函数
        return jsonify({'msg': 'get请求'})

    def post(self):  # 重写Resource父类中的post函数
        return jsonify({'msg': 'post请求'})

# ----------------------------字段格式化---------------------------- #

# Flask-RESTful
# 字段格式化：定义返回给前端的数据格式


ret_fields = {
    'status': fields.Integer,
    'msg': fields.String,
    'like': fields.String(default='chicken'),
    'like1': fields.String(),
    # 'data': fields.String
    'dd': fields.String(attribute='data')   # dd返回值使用data的值
}


class UserResource(Resource):
    @marshal_with(ret_fields)
    def get(self):
        return {
            'status': 1,
            'msg': 'ok',
            'data': '这是一段字符串'
        }

# ----------------------------字段格式化---------------------------- #


user_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'age': fields.Integer,
    # absolute=True表示Url的显示是完整路径，点击可以直接跳转
    'url': fields.Url(endpoint='id', absolute=True),  # 搭配urls文件中的跳转路径使用
}


ret_fields2 = {
    'status': fields.Integer,
    'msg': fields.String,
    'data': fields.Nested(user_fields), # Nested adj. 嵌套的
}


class User2Resource(Resource):
    @marshal_with(ret_fields2)
    def get(self):
        user = Student.query.first()
        return {
            'status': 1,
            'msg': 'ok',
            'data': user
        }

# ----------------------------字段格式化---------------------------- #


user_fields1 = {
    'name': fields.String,
    'age': fields.Integer,
}
ret_fields3 = {
    'status': fields.Integer,
    'msg': fields.String,
    'data': fields.List(fields.Nested(user_fields1)) # data里面是列表，列表中的元素是一个一个的user_fields1对象
}

class User3Resource(Resource):
    @marshal_with(ret_fields3)
    def get(self):
        users = Student.query.all()
        return {
            'status': 1,
            'msg': 'ok',
            'data': users
        }

# ----------------------------参数解析---------------------------- #
# 参数解析：解析前端发送过来的数据


parser = reqparse.RequestParser()
parser.add_argument('name', type=str, required=True, help='name是必须参数')


class User4Resource(Resource):
    def get(self):
        # 获取参数
        args = parser.parse_args()
        name = args.get('name')
        return jsonify({'name': name})