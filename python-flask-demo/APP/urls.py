from .exts import api
from .apis import *


api.add_resource(HellowResouce,'/rests/hello')
api.add_resource(UserResource, '/rests/user/', endpoint='id') # endpoint='id',对应apis文件中的fields.Url使用
api.add_resource(User2Resource, '/rests/user2/')  # 给前端页面返回后端数据表中的一个数据
api.add_resource(User3Resource, '/rests/user3/')  # 给前端页面返回后端表中的所有数据
api.add_resource(User4Resource, '/rests/user4/')  # 演示参数解析