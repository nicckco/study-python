import requests
import os,time,json

_SEARCH_TEXT_LIST = ['学习']
_FILTER_TEXT_LIST = []

_ASMR_URL = 'https://www.abc.com'

headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
    'Content-Type':'application/json;charset=UTF-8'
}

_DIR_LIST_ = []

def GET_DIT_LIST():
    return _DIR_LIST_

def SET_DIR_LIST():
    global _DIR_LIST_
    _DIR_LIST_ = []

_IMAGE_URL_LIST_ = []

def SET_IMAGE_URL_LIST():
    global _IMAGE_URL_LIST_
    _IMAGE_URL_LIST_ = []

def search_asmr(search_text,page):
    url = _ASMR_URL + '/api/fs/search'
    limit = 100
    param = {
        "parent":"/asmr",
        "keywords":search_text,
        "scope":0,
        "page":page,
        "per_page":limit,
        "password":""
    }
    

    response = requests.post(url=url, data=json.dumps(param), headers=headers)
    if response.status_code == 200:
        result = response.json()
        if not result['code'] == 200:
            raise Exception('请求出错：%s' %result["message"])
        data = result["data"]
        total = data['total']

        if total > 0:
            content = data["content"]
            for i in range(len(content)):
                item = content[i]
                is_dir = item['is_dir']
                parent = item["parent"]
                name = item["name"]
                if is_dir and parent not in _DIR_LIST_ and 'mp3' in name:
                    _DIR_LIST_.append(parent)
                else:
                    path = parent + '/' + name
                    get_asmr(path)
        size = total / limit + 1 if total % limit > 0  else total / limit
        if page < size:
            search_asmr(search_text,page + 1)


def get_asmr(path):
    url = _ASMR_URL + '/api/fs/get'
    param = {
        "path":path,
        "password":""
    }
    response = requests.post(url=url, data=json.dumps(param), headers=headers)
    if response.status_code == 200:
        if not result['code'] == 200:
            raise Exception('请求出错：%s' %result["message"])
        result = response.json()
        data = result["data"]
        raw_url = data['raw_url']
        if not checkInUrl(raw_url) and raw_url not in  _IMAGE_URL_LIST_:
                        _IMAGE_URL_LIST_.append(raw_url)


def list_asmr(dir_path,page):
    limit = 30
    param = {
        "path":dir_path,
        "password":"",
        "page":page,
        "per_page":limit,
        "refresh":False
    }
    url = _ASMR_URL + '/api/fs/list'
    response = requests.post(url, data=json.dumps(param), headers=headers)
    if response.status_code == 200:
        result = response.json()
        if not result['code'] == 200:
            raise Exception('请求出错：%s' %result["message"])
        data = result["data"]
        total = data['total']

        if total > 0:
            content = data["content"]
            for i in range(len(content)):
                item = content[i]
                name = item["name"]
                sign = item["sign"]
                is_dir = item['is_dir']
                if is_dir:
                    list_asmr(dir_path + '/' + name,1)
                else:
                    download_url =  _ASMR_URL + '/d' + dir_path + '/' + name + '?sign=%s' % sign

                    if not checkInUrl(download_url) and download_url not in  _IMAGE_URL_LIST_:
                        _IMAGE_URL_LIST_.append(download_url)

        size = total / limit + 1 if total % limit > 0  else total / limit
        if page < size:
            list_asmr(dir_path,page + 1)


def checkInUrl(url):
    for i in _FILTER_TEXT_LIST:
        if i in url:
            return True
    return False

def spide():
    SET_IMAGE_URL_LIST()
    SET_DIR_LIST()
    for search_text in _SEARCH_TEXT_LIST:
        search_asmr(search_text,1)

    
    for dir_path in _DIR_LIST_:
        list_asmr(dir_path,1)

    
    root_path = os.getcwd() + '/output/asmr/asmrgay'
    if not os.path.exists(root_path):
        os.makedirs("")

    with open("%s/download-%d.txt" %(root_path,int(time.time())), "w") as f:
        for url in _IMAGE_URL_LIST_:
            f.writelines(url)


if __name__ == "__main__":
    spide()