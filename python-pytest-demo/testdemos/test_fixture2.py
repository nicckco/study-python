import pytest

@pytest.fixture(scope="function")
def scope_function():
    print("\nthis is a scope function fixture")
    return "function"


class Test_Case:
    def test_1(self, scope_function):
        print("进入test1")
        name = 'function'
        assert scope_function == name
    
    def test_2(self, scope_function):
        print("进入test2")
        sex = 'function'

if __name__ == "__main__":
    pytest.main(['-vs', 'testdemos/test_fixture2.py'])