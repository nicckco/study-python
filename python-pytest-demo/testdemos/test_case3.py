import pytest

@pytest.mark.xfail(reason="预计测试失败，结果测试成功1====1")
def test_one():
    assert  1 == 1

@pytest.mark.xfail(reason='预计测试失败，结果测试失败1====2')
def test_two():
    assert 2 == 1

def test_three():
    pytest.xfail(reason="预计测试失败，后面的不会执行")
    assert 22 == 22


@pytest.mark.xfail(condition= True,reason="预计测试成功,结果测试失败")
def test_four():
    print('1231231231231')
    assert 1==2

'''
    raises，异常类型参数，默认值为None，可以是单个异常，也可以是多个异常组成的元组；
    如果测试用例执行失败，出现的异常类型在raises里，则不会抛出异常，测试用例标记为XFAIL；
    如果测试用例执行失败，出现raises之外的异常，则测试用例标记为FAILED，并抛出异常信息。
'''
@pytest.mark.xfail(raises = ZeroDivisionError,reason="测试失败，但一场不匹配,抛出异常")
def test_five():
    assert 1 == 2


'''
    run，布尔型参数，默认值为True；
    当run=False时候，直接将测试用例标记为XFAIL，不需要执行测试用例。
'''
@pytest.mark.xfail(run = False)
def test_six():
    # 后面的代码不会执行
    print('run=========')


'''
    strict，关键字参数，默认值为False；
    当strict=False时，如果用例执行失败，结果标记为XFAIL，表示符合预期的失败；如果用例执行成功，结果标记为XPASS，表示不符合预期的成功；
    当strict=True时，如果用例执行成功，结果将标记为FAILED，而不再是XPASS了；
    可以在pytest.ini文件中配置：xfail_strict=true。
'''
@pytest.mark.xfail(strict=True)
def test_seven():
    assert 1 == 1


if __name__ == "__main__":
    pytest.main(['-vs','testdemos/test_case3.py'])