import pytest


@pytest.fixture(scope="function")
def scope_function():
    print("\n进入 scope function")
    return "function"

@pytest.fixture(scope = 'class')
def scope_class():
    print("\n进入 Test class,只执行一次")
    return 'class'


class Test_Case:
    def test_one(self,scope_class,scope_function):
        print('进入test_one')
        assert scope_class == 'class'
        assert scope_function == "function"

    def test_two(self,scope_class,scope_function):
        print('进入test_two')
        assert scope_class == 'class'

if __name__ == "__main__":
    pytest.main(['-vs','testdemos/test_fixture3.py'])