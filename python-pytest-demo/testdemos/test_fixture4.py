import pytest


@pytest.fixture(scope="function")
def scope_function():
    print("进入 function scope")
    return "function"

@pytest.fixture(scope = 'class')
def scope_class():
    print("进入 class scope,只执行一次=======")
    print("")
    return 'class'

@pytest.fixture(scope = 'module')
def scope_module():
    print("\n进入module scope,在当前py文件下，只执行一次======")
    print("")
    return "module"


class Test_Case:
    def test_one(self,scope_class,scope_function,scope_module):
        print('进入test_one')
        assert scope_class == 'class'
        assert scope_function == "function"
        assert scope_module == "module"

    def test_two(self,scope_class,scope_function,scope_module):
        print('进入test_two')
        assert scope_class == 'class'

if __name__ == "__main__":
    pytest.main(['-vs','testdemos/test_fixture4.py'])