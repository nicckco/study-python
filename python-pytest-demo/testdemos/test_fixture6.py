import pytest


@pytest.fixture(name='short')
def this_is_so_long_function_fixture():
    return 'short'

def test_one(short):
    print("short == %s" %short)
    assert short == "short"


if __name__ == "__main__":
    pytest.main(['-vs', 'testdemos/test_fixture6.py'])