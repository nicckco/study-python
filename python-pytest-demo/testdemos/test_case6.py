import pytest,warnings


def apiv1():
    warnings.warn(UserWarning('api v1'))
    return 1

def test_one():
    assert apiv1() == 1


'''
    警告过滤器filterwarnings装饰的测试函数test_two过滤掉了匹配到 "api v1" 开头的警告信息。
'''
@pytest.mark.filterwarnings("ignore:api v1")
def test_two():
    assert apiv1() == 1

if __name__ == "__main__":
    pytest.main(['-vs','testdemos/test_case6.py'])