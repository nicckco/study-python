import pytest

@pytest.fixture()
def fixtrue_one():
    print("this is fixtrue one")


@pytest.mark.usefixtures("fixtrue_one")
class Test_One:
    def test_two(self):
        pass

    def test_three(self):
        pass

if __name__ == "__main__":
    pytest.main(['-vs','testdemos/test_case5.py'])
