import pytest

'''
    一个参数，对应一个值列表
'''
@pytest.mark.parametrize("a", [12])
def test_one(a):
    print('一个参数===============')
    print('参数a的值为：',a)
    print('一个参数===============')


'''
    两个参数
'''
@pytest.mark.parametrize('a,b',[(12,13)])
def test_two(a,b):
    print('两个参数===============')
    print("a的参数为：%d，b的参数为：%d" %(a,b))
    print('两个参数===============')


'''
    两组参数
'''
@pytest.mark.parametrize("a,b,c",[(12,13,14),(13,14,15)])
def test_three(a,b,c):
    print('两组参数===============')
    print("a的参数为：%d，b的参数为：%d，c的参数为：%d" %(a,b,c))
    print('两组参数===============')
    

'''
    多组参数
'''
@pytest.mark.parametrize("a,b,c",[(12,13,14),(21,22,23),(33,34,35)])
def test_four(a,b,c):
    print('多组参数===============')
    print("a的参数为：%d，b的参数为：%d，c的参数为：%d" %(a,b,c))
    print('多组参数===============')


if __name__ == "__main__":
    pytest.main(['-vs','testdemos/test_case4.py'])