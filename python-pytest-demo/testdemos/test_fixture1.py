import pytest

@pytest.fixture()
def fixture1():
    a = 'zhangsan'
    return a

@pytest.fixture()
def fixture2():
    return 'zhangsan','lisi'

@pytest.fixture()
def fixture3(fixture1):
    return fixture1,'wangwu'

def test_a(fixture1):
    assert fixture1 == 'zhangsan'
    print('fixture传入一条数据')

def test_b(fixture2):
    assert fixture2[0] == 'zhangsan'
    assert fixture2[1] == "lisi"
    print(str(fixture2))
    print('fixture传入一组数据')

def test_c(fixture1, fixture2):
    assert fixture2[0] == fixture1
    print('多个fixture作为参数传入')

def test_d(fixture3):
    print(str(fixture3))
    print('fixture相互传递')

if __name__ == "__main__":
    pytest.main(['-vs','testdemos/test_fixture1.py'])