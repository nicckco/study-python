import pytest

@pytest.fixture(autouse=True)
def common_fixture():
    print("这个是第二级公用模块===second===")


@pytest.fixture(scope="session")
def scope_session():
    print("this is session scope fixture")
    return 'session'