import pytest
import allure



@allure.epic('附加')
@allure.feature('附加中心')
class Test_Catch():

    
    @allure.title('附加文本')
    def test_attach_text(self):
        allure.attach("纯文本", attachment_type=allure.attachment_type.TEXT)
 
 
    @allure.title('附加html')
    def test_attach_html(self):
        allure.attach("<body>这是一段htmlbody块</body>", "html页面", attachment_type=allure.attachment_type.HTML)
    
    
    @allure.title('附加图片')
    def test_attach_photo(self):
        allure.attach.file("./APP/files/catch.jpg", name="图片", attachment_type=allure.attachment_type.JPG)