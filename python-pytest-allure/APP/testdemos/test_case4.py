import pytest
import allure
'''
    本文件案例主要是核心为：标志安全级别
'''


@allure.epic('安全')
@allure.feature('安全中心')
class Test_safe():

    @allure.story("安全级别")
    @allure.title('Trivial:不重要')
    @allure.severity(allure.severity_level.TRIVIAL)
    def test_safe_trivial(self):
        pass

    
    @allure.story("安全级别")
    @allure.title('Minor:不太重要')
    @allure.severity(allure.severity_level.MINOR)
    def test_safe_Minor(self):
        pass

    
    @allure.story("安全级别")
    @allure.title('Normal:正常')
    @allure.severity(allure.severity_level.NORMAL)
    def test_safe_Normal(self):
        pass
    
    @allure.story("安全级别")
    @allure.title('Critical:严重')
    @allure.severity(allure.severity_level.CRITICAL)
    def test_safe_Critical(self):
        pass

    @allure.story("安全级别")
    @allure.title('Blocker:严重')
    @allure.severity(allure.severity_level.BLOCKER)
    def test_safe_Blocker(self):
        pass