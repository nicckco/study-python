import pytest
import allure


'''
    本文件案例主要是核心为：对测试案例进行分级分类命名
'''

@allure.epic('会员')
@allure.feature("会员中心")
class Test_MemberCenter():

    @allure.story('登录')
    @allure.title('密码登录')
    def test_login_pwdLogin(self):
        with allure.step('校验账户名'):
            pass
        with allure.step("校验密码"):
            pass
        with allure.step('登录成功'):
            pass
        pass

    @allure.story('登录')
    @allure.title('手机号登录')
    def test_login_mobileLogin(self):
        with allure.step('校验手机号'):
            pass
        with allure.step("校验密码"):
            print("密码错误！")
            assert 1 == 2
        with allure.step('登录成功'):
            pass
        pass


    @allure.story('退出')
    @allure.title('退出登录')
    @allure.step('退出步骤')
    def test_out_login(self):
        pass
    

@allure.epic("会员")
@allure.story("会员数据")
class Test_MemberData():
    
    def test_data(self):
        pass