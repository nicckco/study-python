import allure
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
 


@allure.epic('selenium')
@allure.feature('web中心')
class Test_Web():

    @allure.title("效果展示")
    def test_steps_demo(self):
        # 注：如果无法打开chrome,可以是GitHub无法访问，尝试开一下梯子，如果没有，就多等一会吧，就是会比较慢
        driver = webdriver.Chrome()
        driver.get("https://www.baidu.com")
        driver.find_element(By.ID,"kw").send_keys('allure')
        driver.find_element(By.ID,"su").click()
        time.sleep(3)
        driver.save_screenshot("./APP/files/catch2.png")

        allure.attach.file("./APP/files/catch2.png", attachment_type=allure.attachment_type.PNG)
        # allure.attach('<head></head><body>首页</body>', 'Attach with HTML type', allure.attachment_type.HTML)