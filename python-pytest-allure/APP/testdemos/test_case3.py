import pytest
import allure


'''
    本文件案例主要是核心为：链接跳转
'''

@allure.epic('链接')
@allure.feature('链接中心')
class Test_Link():

    @allure.story("跳转链接")
    @allure.title('跳转百度')
    @allure.link("https://www.baidu.com/", name="百度URL")
    def test_link_go_baidu(self):
        pass

    
    @allure.story("跳转链接")
    @allure.title('跳转阿里云')
    @allure.issue("https://help.aliyun.com/zh/ecs/?spm=0.0.0.0/", name="阿里云帮助中心")
    def test_link_issue_aliyun(self):
        pass

    
    @allure.story("跳转链接")
    @allure.title('跳转京东')
    @allure.testcase("https://jd.com/", name="跳转京东")
    def test_link_testcase_jd(self):
        pass