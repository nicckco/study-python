import pytest


'''
    本文件案例主要是核心为：基本结构搭建，案例展示
'''

@pytest.mark.parametrize("name,age,sex", [('张三',22,'男孩')])
def test_one(name,age,sex):
    print('我的名字叫%s,我今年%d岁了，我是一个%s' %(name,age,sex))
    pass


@pytest.mark.parametrize("a,b", [(22,15),(20,24)])
def test_two(a,b):
    assert a > b
